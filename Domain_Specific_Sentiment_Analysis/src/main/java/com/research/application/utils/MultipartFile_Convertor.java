package com.research.application.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MultipartFile_Convertor {

    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File( file.getOriginalFilename());
        file.transferTo(convFile);
        return convFile;
    }
}
