package com.research.application;

import com.research.application.sentiment_analysis.fast_text.FastText_Classifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomainSpecificSentimentAnalysisApplication {

	public static void main(String[] args) {

		SpringApplication.run(DomainSpecificSentimentAnalysisApplication.class, args);
		FastText_Classifier.loadModel();
	}
}
