package com.research.application.ontology;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

public class Neo4j_Configuratation {

	private static String URL;
	private static String USERNAME;
	private static String PASSWORD;
	private static Properties property = null;
	private static Driver driver = null;

	public static void loadPropertyFile() {
		try {
			InputStream inputStream = new FileInputStream("neo4j.properties");
			property = new Properties();
			property.load(inputStream);

			URL = property.getProperty("neo4j.uri");
			USERNAME = property.getProperty("neo4j.username");
			PASSWORD = property.getProperty("neo4j.password");
		} catch (FileNotFoundException e1) {
			System.err.println(e1);

		} catch (IOException e) {
			System.err.println(e);
		}

	}

	public static void setDriver() {

		loadPropertyFile();
		driver = GraphDatabase.driver(URL, AuthTokens.basic(USERNAME, PASSWORD));
	}

	public static Driver getDriver() {

		return driver;
	}

}
