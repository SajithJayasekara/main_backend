package com.research.application.ontology.data_retrieve_dao;

import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import com.research.application.ontology.Neo4j_Configuratation;

public class Data_Retrieve {

    private static Driver driver = null;
    private Session session;

    public static void setDriver() {

        driver = Neo4j_Configuratation.getDriver();

    }

    public void setSession() {
        session = driver.session();
    }


    public Double getIncrease(String DOMAIN, String text) {
        Double value = null;
        StatementResult result = session.run("MATCH (n:"+DOMAIN+" {name:'" + text + "'}) return n.increase AS sentiment");

        while (result.hasNext()) {
            Record record = result.next();
            if (record.get("sentiment").isNull()) {

                return value;
            } else {
                value = record.get("sentiment").asDouble();
            }

        }


        return value;
    }

    public Double getDecrease(String DOMAIN, String text) {
        Double value = null;
        StatementResult result = session.run("MATCH (n:"+DOMAIN+" {name:'" + text + "'}) return n.decrease AS sentiment");

        while (result.hasNext()) {
            Record record = result.next();
            if (record.get("sentiment").isNull()) {

                return value;
            } else {
                value = record.get("sentiment").asDouble();
            }

        }


        return value;
    }

    public void closeSession() {
        session.close();
    }

    public static void closeDriver() {


        driver.close();
    }

}
