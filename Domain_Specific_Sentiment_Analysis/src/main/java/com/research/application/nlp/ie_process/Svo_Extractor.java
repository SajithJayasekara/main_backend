package com.research.application.nlp.ie_process;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class Svo_Extractor {

    static String sent3 = "A CTB bus plying from Anuradhapura to Padaviya was caught in the LTTE claymore mine on Wednesday around 8.15 pm killing 15 civilians including two soldiers and three women and injuring 23 others";
    static Set<String> objectSet = new HashSet<String>();
    static Set<String> subjectset = new HashSet<String>();
    private static ArrayList<String> objectAL = new ArrayList<String>();
    private static ArrayList<String> subjectAL = new ArrayList<String>();
    private static int sentenceId;

    public ArrayList<String> getObjects(String contentOfDocument[]) {

        String sentences[] = contentOfDocument;
        for(int i=0;i<sentences.length;i++){
            objectSet.clear();
            String sentence = sentences[i];
            sentenceId = 1+i;

            //get objects from parser tree
            LexicalizedParser lp = LexicalizedParser.loadModel("nlp_lib\\englishPCFG.ser");
            Tree parse = null;
            TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(
                    new CoreLabelTokenFactory(), "");
            Tokenizer<CoreLabel> tok = tokenizerFactory.getTokenizer(new StringReader(sentence));
            List<CoreLabel> rawWords2 = tok.tokenize();
            parse = lp.apply(rawWords2);
            TreebankLanguagePack tlp = new PennTreebankLanguagePack();
            GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
            GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
            List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();

            Object[] list = tdl.toArray();
            TypedDependency typedDependency;
            for (Object object : list)
            {
                typedDependency = (TypedDependency) object;

                if((typedDependency.reln().toString() == "dobj")) {
                    objectSet.add(typedDependency.dep().word()+ "_"+typedDependency.gov().word());
                }}
        }
        return objectAL;
    }

    public ArrayList<String> getSubjects(String contentOfDocument[]) {

        String sentences[] = contentOfDocument;
        for(int i=0;i<sentences.length;i++){
            objectSet.clear();
            String sentence = sentences[i];
            sentenceId = 1+i;

            //get objects from parser tree
            LexicalizedParser lp = LexicalizedParser.loadModel("nlp_lib\\englishPCFG.ser");
            Tree parse = null;
            TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(
                    new CoreLabelTokenFactory(), "");
            Tokenizer<CoreLabel> tok = tokenizerFactory.getTokenizer(new StringReader(sentence));
            List<CoreLabel> rawWords2 = tok.tokenize();
            parse = lp.apply(rawWords2);
            TreebankLanguagePack tlp = new PennTreebankLanguagePack();
            GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
            GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
            List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();

            Object[] list = tdl.toArray();
            TypedDependency typedDependency;
            for (Object object : list)
            {
                typedDependency = (TypedDependency) object;

                if((typedDependency.reln().toString() == "nsubj") ||(typedDependency.reln().toString() == "csubj") ||
                        (typedDependency.reln().toString() == "nsubjpass")) {
                    subjectset.add(typedDependency.dep().word()+ "_"+typedDependency.gov().word());
                }}
        }
        return subjectAL;

    }
}
