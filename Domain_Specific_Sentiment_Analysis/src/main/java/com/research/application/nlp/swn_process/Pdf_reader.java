package com.research.application.nlp.swn_process;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import opennlp.tools.util.InvalidFormatException;
import com.research.application.nlp.swn_process.Pos_extractor;
import com.research.application.nlp.swn_process.Sentence_Detector;
import com.research.application.nlp.swn_process.Xml_builder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Pdf_reader {
    public static String file005 = "articles\\article_005.pdf" ;


    public static ArrayList<String> xmlRead = new ArrayList<String>();
    public static ArrayList<String> wordList = new ArrayList<String>();

    public static String readSwnPDFs(File filename) throws IOException {
        String content = null;
        PDDocument document = PDDocument.load(filename);
        document.getClass();
        if (!document.isEncrypted()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);
            PDFTextStripper pdfStripper = new PDFTextStripper();
            String pdfFileInText = pdfStripper.getText(document);
            content = pdfFileInText;
        }
        document.close();
        return content;
    }

}
