package com.research.application.nlp.ie_process;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

import com.research.application.nlp.ie_process.Stopword_extractor;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class Word_extractor {
	private int sentenceId;
	ArrayList<String> wordList = new ArrayList<String>();
	
	public ArrayList<String> wordExtractor(String contentArraynew[]){
		
		InputStream tokenModelIn = null;
		InputStream posModelIn = null;
		
		try {
			String sentences[] = contentArraynew;
			for (int i = 0; i < sentences.length; i++) {

				String sentence = sentences[i];
				tokenModelIn = new FileInputStream("nlp_lib\\en-token.bin");
				TokenizerModel tokenModel = new TokenizerModel(tokenModelIn);
				Tokenizer tokenizer = new TokenizerME(tokenModel);
				String tokens[] = tokenizer.tokenize(sentence);
				
				posModelIn = new FileInputStream("nlp_lib\\en-pos-maxent.bin");
				POSModel posModel = new POSModel(posModelIn);
				POSTaggerME posTagger = new POSTaggerME(posModel);
				String tags[] = posTagger.tag(tokens);
				
				InputStream dictLemmatizer = new FileInputStream("nlp_lib\\en-lemmatizer.txt");
	            DictionaryLemmatizer lemmatizer = new DictionaryLemmatizer(dictLemmatizer);
	            String[] lemmas = lemmatizer.lemmatize(tokens, tags);
				sentenceId = 1+i;
				Stopword_extractor stopWord = new Stopword_extractor();
				
				
				for(int token=0;token<tokens.length;token++){
					String checkStopWord = stopWord.remove_stopwords(lemmas[token]);
					if(checkStopWord == "notfound") {
						
						if(lemmas[token]!= "O") {
							String word = sentenceId+"_"+lemmas[token]+"_"+tags[token];
							wordList.add(word);
						}
					}
				}
			} 
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return wordList;
	}
}

