package com.research.application.nlp.ie_process;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

public class Sentence_detector {

	    public String[] sentenceDetect(String content) throws IOException {
	        String paragraph = content;  
	        InputStream inputstream = new FileInputStream("nlp_lib\\en-sent.bin");
	        SentenceModel model = new SentenceModel(inputstream);
	        SentenceDetectorME sdetector = new SentenceDetectorME(model);
	        String sentences[] = sdetector.sentDetect(paragraph);   
	        return sentences;
	    }
}