package com.research.application.nlp.ie_process;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Opinion_extractor {
	
	public String NegativeExtractor(String cheksum) throws FileNotFoundException {
	
		String opinion = "notfound";
		Scanner file = new Scanner(new File("nlp_lib\\negative-words.txt"));
		 while(file.hasNextLine())           
	        {
	            String line = file.nextLine();
	            if(line.equals(cheksum))
	            {
	                opinion = "decrease";
	                break;
	            }
	            else
	            {
	                opinion =  "notfound";
	                continue;
	            }
	        }
		 file.close();
		return opinion;
	}
	public String positiveExtractor(String cheksum) throws FileNotFoundException {
		
		String opinion = "notfound";
		Scanner file = new Scanner(new File("nlp_lib\\positive-words.txt"));
		 while(file.hasNextLine())           
	        {
	            String line = file.nextLine();
	            if(line.equals(cheksum))
	            {
	                opinion = "increase";
	                break;
	            }
	            else
	            {
	                opinion =  "notfound";
	                continue;
	            }
	        }
		 file.close();
		return opinion;
	}
	
}
