package com.research.application.nlp.ie_process;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XML_builder {

    public ArrayList<String> obList;
    public ArrayList<String> subList;

	private int index ;
	private ArrayList<String> wordlistArr = new ArrayList<String>();

	public void xml_builder(ArrayList<String> xmlRead, String[] contentArray, ArrayList<String> wordArray) {
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			doc.appendChild(rootElement);

			index = 1;
			for (String mld : contentArray) {

				//sentence content
				Element sentence_content = doc.createElement("sentenceContent");
				rootElement.appendChild(sentence_content);

				String newIndex = this.getNumbers(index);
				//sentence
				String id = "sen_" + newIndex;
				Element sentence = doc.createElement("sentence");
				sentence.appendChild(doc.createTextNode(mld));
				sentence_content.appendChild(sentence);
				Attr attr = doc.createAttribute("id");
				attr.setValue(id);
				sentence.setAttributeNode(attr);

				//wordlist
				Element wordList = doc.createElement("wordlist");
				sentence_content.appendChild(wordList);

				for (String wordelem : wordArray) {
					String wordlistArray[] = wordelem.split("_");

					if (Integer.parseInt(wordlistArray[0]) == index) {
						String postag = wordlistArray[2];
						String newwords = wordlistArray[1];
						Element word = doc.createElement("word");
						word.appendChild(doc.createTextNode(newwords));
						wordList.appendChild(word);
						Attr postagAttr = doc.createAttribute("pos");
						postagAttr.setValue(postag);
						word.setAttributeNode(postagAttr);

					}
				}

				//subject and object of sentence
                Element sentence_so = doc.createElement("subjobj");
                sentence_content.appendChild(sentence_so);

                ArrayList<String> senObjList = new ArrayList<>();
                //set object
                for(String objelem: wordArray){
				    String objlistArray[] = objelem.split("_");

				    if(Integer.parseInt(objlistArray[0]) == index){
                        senObjList.add(objlistArray[1]);
                    }
                }

                String  objectName = senObjList.get(0);
                Element objects = doc.createElement("object");
                objects.appendChild(doc.createTextNode(objectName));
                sentence_so.appendChild(objects);

                ArrayList<String> senSubList = new ArrayList<>();
                //set object
                for(String subelem: wordArray){
                    String SublistArray[] = subelem.split("_");

                    if(Integer.parseInt(SublistArray[0]) == index){
                        senObjList.add(SublistArray[1]);
                    }
                }

                    String  subName = senObjList.get(senObjList.size()-1);
                    Element subjects = doc.createElement("subject");
                    subjects.appendChild(doc.createTextNode(subName));
                    sentence_so.appendChild(subjects);



				//senetence pattern
				Element sentence_pattern = doc.createElement("sentencePatterns");
				sentence_content.appendChild(sentence_pattern);

				for (String ml : xmlRead) {

					String array1[] = ml.split("_");

					if (Integer.parseInt(array1[0]) == index) {
						String noun = array1[1];

						if (array1[2] == "decrease" || array1[2] == "increase") {
							String relation = array1[2];

							Element objectPattern = doc.createElement("pattern");
							sentence_pattern.appendChild(objectPattern);

							Element object = doc.createElement("object");
							object.appendChild(doc.createTextNode(noun));
							objectPattern.appendChild(object);

							Element rel = doc.createElement("relation");
							rel.appendChild(doc.createTextNode(relation));
							objectPattern.appendChild(rel);
						} else {
							String genaralRelation[] = {"decrease", "increase"};

							for (String rela : genaralRelation) {
								Element objectPattern = doc.createElement("pattern");
								sentence_pattern.appendChild(objectPattern);

								Element object = doc.createElement("noun");
								object.appendChild(doc.createTextNode(noun));
								objectPattern.appendChild(object);

								Element rel = doc.createElement("relation");
								rel.appendChild(doc.createTextNode(rela));
								objectPattern.appendChild(rel);
							}
						}
					}
				}
				index++;
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("output_001.xml"));

			transformer.transform(source, result);

			System.out.println("file saved sucessfully");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}

	}

	public String getNumbers(int id){
		int num = id;
		String formatted = String.format("%03d",num);
		return formatted;
	}

    public void setObList(ArrayList<String> obList) {
        this.obList = obList;
    }

    public void setSubList(ArrayList<String> subList) {
        this.subList = subList;
    }
}
