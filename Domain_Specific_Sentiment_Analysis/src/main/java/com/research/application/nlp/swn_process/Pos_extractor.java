package com.research.application.nlp.swn_process;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.research.application.nlp.swn_process.Stop_word_extractor;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class Pos_extractor {
    private int sentenceId;
    Set<String> wordset = new HashSet<String>();
    private ArrayList<String> wordMap = new ArrayList<String>();

    public void main(String[] args) throws IOException {
    }
    public ArrayList<String> postagging(String contentArraynew[]){
        InputStream tokenModelIn = null;
        InputStream posModelIn = null;

        try {

            String sentences[] = contentArraynew;
            for(int i=0;i<sentences.length;i++){
                wordset.clear();
                String sentence = sentences[i];
                tokenModelIn = new FileInputStream("nlp_lib\\en-token.bin");
                TokenizerModel tokenModel = new TokenizerModel(tokenModelIn);
                Tokenizer tokenizer = new TokenizerME(tokenModel);
                String tokens[] = tokenizer.tokenize(sentence);

                posModelIn = new FileInputStream("nlp_lib\\en-pos-maxent.bin");
                POSModel posModel = new POSModel(posModelIn);
                POSTaggerME posTagger = new POSTaggerME(posModel);
                String tags[] = posTagger.tag(tokens);

                InputStream dictLemmatizer = new FileInputStream("nlp_lib\\en-lemmatizer.txt");
                DictionaryLemmatizer lemmatizer = new DictionaryLemmatizer(dictLemmatizer);
                String[] lemmas = lemmatizer.lemmatize(tokens, tags);

                sentenceId = 1+i;
                Stop_word_extractor stopWord = new Stop_word_extractor();

                for(int j=0; j<tokens.length; j++){
                    String checkStopWord = stopWord.remove_stopwords(lemmas[j]);
                    if(checkStopWord == "notfound") {
                        if((tags[j].contains("NN") ||(tags[j].contains("VB")) || (tags[j].contains("J"))) && (lemmas[j] != "O") ) {
                            wordset.add(lemmas[j]+"_"+tags[j]);
                        }
                    }
                }

                for (String ele: wordset) {
                    String cv = Integer.toString(sentenceId)+"_"+ele;
                    wordMap.add(cv);
                }
            }
        }
        catch (IOException e) {
            // Model loading failed, handle the error
            e.printStackTrace();
        }
        finally {
            if (tokenModelIn != null) {
                try {
                    tokenModelIn.close();
                }
                catch (IOException e) {
                }
            }
            if (posModelIn != null) {
                try {
                    posModelIn.close();
                }
                catch (IOException e) {
                }
            }
        }
        return wordMap;

    }
}
