package com.research.application.nlp.swn_process;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Stop_word_extractor {
    public String remove_stopwords(String checksum) throws FileNotFoundException {
        Scanner file = new Scanner(new File("nlp_lib\\stop_words.txt"));
        String opinion = "notfound";
        while(file.hasNextLine())
        {
            String line = file.nextLine();
//	            if(line.indexOf(checksum) != -1)
            if(line.equals(checksum))
            {
                opinion = "stop-word";
                break;
            }
            else
            {
                opinion =  "notfound";
                continue;
            }
        }
        file.close();
        return opinion;

    }
}
