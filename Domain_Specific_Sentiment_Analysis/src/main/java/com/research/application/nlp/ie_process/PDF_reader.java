package com.research.application.nlp.ie_process;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import com.research.application.nlp.ie_process.Triplet_Extractor;
import com.research.application.nlp.ie_process.Word_extractor;
import com.research.application.nlp.ie_process.XML_builder;

import java.io.*;
import java.util.ArrayList;


public class PDF_reader {

	public static String file005 = "articles\\article_005.pdf" ;

	public static ArrayList<String> xmlRead = new ArrayList<String>();
	public static ArrayList<String> wordList = new ArrayList<String>();


	public static String readPDFs(File filename) throws IOException {
		String content = null;
		PDDocument document = PDDocument.load(filename);
		document.getClass();
		if (!document.isEncrypted()) {

			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
			stripper.setSortByPosition(true);
			PDFTextStripper pdfStripper = new PDFTextStripper();
			String pdfFileInText = pdfStripper.getText(document);
			content = pdfFileInText;
		}
		document.close();

		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("document.txt"), "utf-8"))) {
			writer.write(content);
		}
		return content;
	}

}
