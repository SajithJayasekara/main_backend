package com.research.application.nlp.ie_process;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.research.application.nlp.ie_process.Opinion_extractor;
import com.research.application.nlp.ie_process.Stopword_extractor;
import com.research.application.nlp.ie_process.Svo_Extractor;

import opennlp.tools.lemmatizer.DictionaryLemmatizer;
//import opennlp.tools.namefind.NameFinderME;
//import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
//import opennlp.tools.util.Span;

public class Triplet_Extractor {
	ArrayList<String> objectList = new ArrayList<String>();
	ArrayList<String> subjectList = new ArrayList<String>();

	ArrayList<String> xmlObjectMap = new ArrayList<String>();
	ArrayList<String> xmlSubjMap = new ArrayList<String>();

	ArrayList<String> functionList = new ArrayList<String>();
	ArrayList<String> xmlMap = new ArrayList<String>();

	private int sentenceId;
	private int posCount = 0;
	private int negCount = 0;
	public String identifier;

	Set<String> objectset = new HashSet<String>();

	public void main(String[] args) throws IOException {
	}

	public ArrayList<String> postagging(String contentArraynew[]){
		InputStream tokenModelIn = null;
		InputStream posModelIn = null;

		try {

			String sentences[] = contentArraynew;
			for(int i=0;i<sentences.length;i++){
				objectset.clear();
				String sentence = sentences[i];
				tokenModelIn = new FileInputStream("nlp_lib\\en-token.bin");
				TokenizerModel tokenModel = new TokenizerModel(tokenModelIn);
				Tokenizer tokenizer = new TokenizerME(tokenModel);
				String tokens[] = tokenizer.tokenize(sentence);

				posModelIn = new FileInputStream("nlp_lib\\en-pos-maxent.bin");
				POSModel posModel = new POSModel(posModelIn);
				POSTaggerME posTagger = new POSTaggerME(posModel);
				String tags[] = posTagger.tag(tokens);

				InputStream dictLemmatizer = new FileInputStream("nlp_lib\\en-lemmatizer.txt");
				DictionaryLemmatizer lemmatizer = new DictionaryLemmatizer(dictLemmatizer);
				String[] lemmas = lemmatizer.lemmatize(tokens, tags);

				sentenceId = 1+i;
				Stopword_extractor stopWord = new Stopword_extractor();

				for(int j=0; j<tokens.length; j++){

					String checkStopWord = stopWord.remove_stopwords(lemmas[j]);
					if(checkStopWord == "notfound") {
						if(tags[j].contains("NN")) {
							int k = j+1 ;
							if(tags[k].contains("NN")) {
								String compoundNoun = tokens[j]+" "+tokens[k];
								objectset.add(compoundNoun);
							}
							else if(!lemmas[j].contentEquals("O")) {
								objectset.add(lemmas[j]);
							}
						}
						else if(tags[j].contains("VB")|| tags[j].contains("JJ")) {

							Opinion_extractor ObjExtractor = new Opinion_extractor();
							String negOpinion = ObjExtractor.NegativeExtractor(lemmas[j]);
							String posOpinion = ObjExtractor.positiveExtractor(lemmas[j]);

							if(negOpinion == "decrease"){negCount++;}
							else if(posOpinion == "increase") {posCount++;}
						}
					}
				}

				if(posCount == negCount) {
					identifier = "both";
				}
				else if(posCount > negCount) {
					identifier = "increase";
				}
				else {
					identifier = "decrease";
				}
				for (String ele: objectset) {
					String cv = Integer.toString(sentenceId)+"_"+ele+"_"+identifier ;
					xmlMap.add(cv);
				}
			}
		}
		catch (IOException e) {
			// Model loading failed, handle the error
			e.printStackTrace();
		}
		finally {
			if (tokenModelIn != null) {
				try {
					tokenModelIn.close();
				}
				catch (IOException e) {
				}
			}
			if (posModelIn != null) {
				try {
					posModelIn.close();
				}
				catch (IOException e) {
				}
			}
		}
		return xmlMap;
	}
}
