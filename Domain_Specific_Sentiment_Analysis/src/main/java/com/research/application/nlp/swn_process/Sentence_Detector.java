package com.research.application.nlp.swn_process;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.InvalidFormatException;

public class Sentence_Detector {

    public String[] sentenceDetect(String content) throws InvalidFormatException,  IOException {
        String paragraphContent = content;
        InputStream is = new FileInputStream("nlp_lib\\en-sent.bin");
        SentenceModel model = new SentenceModel(is);
        SentenceDetectorME sdetector = new SentenceDetectorME(model);
        String sentences[] = sdetector.sentDetect(paragraphContent);
        return sentences;
    }
}
