package com.research.application.nlp.swn_process;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Xml_builder {
    int index ;
    ArrayList<String> wordlistArr = new ArrayList<String>();

    public void xml_Builder(ArrayList<String> xmlReads, String[] contentArray) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("document");
            doc.appendChild(rootElement);


            index = 1;
            for(String mld: contentArray) {
                String newIndex = this.getNumbers(index);

                //sentence content
                String scid = "sen_"+newIndex;
                Element sentence_content = doc.createElement("sentenceContent");
                rootElement.appendChild(sentence_content);
                Attr scattr = doc.createAttribute("id");
                scattr.setValue(scid);
                sentence_content.setAttributeNode(scattr);

                //sentence
                Element sentence = doc.createElement("sentence");
                sentence.appendChild(doc.createTextNode(mld));
                sentence_content.appendChild(sentence);

                //wordlist
                Element wordlist = doc.createElement("wordlist");
                sentence_content.appendChild(wordlist);

                for(String ml: xmlReads) {

                    String array1[]= ml.split("_");
                    if(Integer.parseInt(array1[0]) == index ) {
                        String contentWord = array1[1];
                        String posTag = array1[2];

                        Element word = doc.createElement("word");
                        word.appendChild(doc.createTextNode(contentWord));
                        wordlist.appendChild(word);
                        Attr wattr = doc.createAttribute("pos");
                        wattr.setValue(posTag);
                        word.setAttributeNode(wattr);
                    }

                }
                index++;
            }


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("swnXML.xml"));

            transformer.transform(source, result);

            System.out.println("file saved sucessfully");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public String getNumbers(int id){
        int num = id;
        String formatted = String.format("%03d",num);
        return formatted;
    }
}
