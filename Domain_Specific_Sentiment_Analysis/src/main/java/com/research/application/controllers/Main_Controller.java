package com.research.application.controllers;

import com.research.application.nlp.ie_process.*;
import com.research.application.sentiment_analysis.analyser.Analyser;
import com.research.application.sentiment_analysis.analyser.Hybrid_Analyser;
import com.research.application.sentiment_analysis.analyser.SentiwordNet_Analyser;
import com.research.application.sentiment_analysis.fast_text.FastText_Classifier;
import com.research.application.sentiment_analysis.model.Sentiment_Result;
import com.research.application.sentiment_analysis.result_generator.Result_Generator;
import com.research.application.sentiment_analysis.result_generator.Result_Generator_General;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.research.application.nlp.swn_process.Pdf_reader;
import com.research.application.nlp.swn_process.Pos_extractor;
import com.research.application.nlp.swn_process.Sentence_Detector;
import com.research.application.nlp.swn_process.Xml_builder;
import com.research.application.nlp.ie_process.Svo_Extractor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class Main_Controller {



    private ArrayList<Sentiment_Result> results;
    private static String SentiWordNet_PATH = "SentiWordNet_3.0.0_20130122.txt";
    private static String XML_PATH = "swnXML.xml";
    private static String XML_PATH_HYBRID = "output_001.xml"; //output_005-v0.xml output_001.xml
    private String DOMAIN;
    private String FASTTEXT = "Disable";
    private File inputFile = null;

    private static ArrayList<String> xmlRead = new ArrayList<String>();
    private static ArrayList<String> wordList = new ArrayList<String>();
    private static ArrayList<String> objectArList = new ArrayList<String>();
    private static ArrayList<String> subjectArList = new ArrayList<String>();

    /**
     * Endpoint which accept document submission
     **/
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/document")
    public void setPdfFile(@RequestPart(value = "file") MultipartFile file) {
        try {

            inputFile = new File(file.getOriginalFilename());
            inputFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(inputFile);
            fos.write(file.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Endpoint which handle sentiment Analysis requests
     **/
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/sentimentAnalysis")
    public ArrayList<Sentiment_Result> sentiment_analysis(@RequestPart(value = "domain") String domain, @RequestPart(value = "fastText") String fastText) {
        DOMAIN = domain;
        FASTTEXT = fastText;

        try {
            if (inputFile != null) {
                if (inputFile.exists()) {

                    /**
                     * Natural Language Process
                     **/

                    //hybrid process
                    PDF_reader objPDFreader = new PDF_reader();
                    String documentContent = objPDFreader.readPDFs(inputFile);
                    Sentence_detector objSentecedetector = new Sentence_detector();
                    String contentArray[] = objSentecedetector.sentenceDetect(documentContent);
                    Triplet_Extractor objtripletExtractor = new Triplet_Extractor();
                    xmlRead = objtripletExtractor.postagging(contentArray);
                    Word_extractor objWordExtractor = new Word_extractor();
                    wordList = objWordExtractor.wordExtractor(contentArray);



                    XML_builder objectXMLbuilder = new XML_builder();
                    objectXMLbuilder.xml_builder(xmlRead, contentArray, wordList);

                    //get object list
                    Svo_Extractor objSvoExtractor = new Svo_Extractor();
                    objectArList = objSvoExtractor.getObjects(contentArray);
                    subjectArList = objSvoExtractor.getSubjects(contentArray);
                    XML_builder objexmlbuilder = new XML_builder();
                    objexmlbuilder.setObList(objectArList);
                    objexmlbuilder.setSubList(subjectArList);

                    //swn process
                    Pdf_reader objectpdfreader = new Pdf_reader();
                    String swnContent = objectpdfreader.readSwnPDFs(inputFile);
                    Sentence_Detector objectSentecedetector = new Sentence_Detector();
                    String swnArray[] = objectSentecedetector.sentenceDetect(swnContent);
                    Pos_extractor objPOSextractor = new Pos_extractor();
                    xmlRead = objPOSextractor.postagging(contentArray);
                    Xml_builder objectXmlbuilder = new Xml_builder();
                    objectXmlbuilder.xml_Builder(xmlRead, swnArray);


                    /**
                     * Sentiment Analysis
                     **/

                    /**
                     * domain specific sentiment analysis process
                     **/
                    // Initialize hybrid analyser
                    Analyser hybridAnalyser = new Hybrid_Analyser(SentiWordNet_PATH, XML_PATH_HYBRID, DOMAIN, FASTTEXT);
                    // perform sentiment analysis
                    HashMap<String, HashMap<String, Double>> sentimentsHashMap_hybrid = hybridAnalyser.analyse();
                    // Initialize result generator
                    Result_Generator result_Generator_hybrid = new Result_Generator_General();
                    // get results for result generator
                    Sentiment_Result result_hybrid = result_Generator_hybrid.generate_result(sentimentsHashMap_hybrid, "Hybrid", DOMAIN);


                    /**
                     * sentiWordNet sentiment analysis process
                     **/
                    // Initialize sentWordNet analyser
                    Analyser analyser = new SentiwordNet_Analyser(SentiWordNet_PATH, XML_PATH);
                    // perform sentiment analysis
                    HashMap<String, HashMap<String, Double>> sentimentsHashMap = analyser.analyse();
                    // Initialize result generator
                    Result_Generator result_Generator = new Result_Generator_General();
                    // get results for result generator
                    Sentiment_Result result = result_Generator.generate_result(sentimentsHashMap, "Sentiwordnet", "SentiWordNet");

                    //results
                    results = new ArrayList<>();
                    results.add(result_hybrid);
                    results.add(result);
                    return results;
                } else {
                    System.out.println(" not exist");
                }

            } else {
                System.out.println(" not ok");
            }
        } catch (IOException er) {
            er.printStackTrace();
        }

        return new ArrayList<>();

    }

    /**
     * Endpoint which handle FastText enable disable requests
     **/
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/createFastTextModel")
    public String setFastTextModel() {
        FastText_Classifier fastText_classifier = new FastText_Classifier();
        fastText_classifier.create_model();
        return "set the model";
    }

}
