package com.research.application.sentiment_analysis.negEx;

import com.research.application.sentiment_analysis.SentiWordNet_handler;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class NegExValidator {

    public static Double negexValidation(String sentence, Double averageValue, Element subObj, SentiWordNet_handler handler) {

        String subject = null;
        String object = null;

        NodeList childNodes = subObj.getChildNodes();


        for (int i = 0; i < childNodes.getLength(); i++) {

            Node node = childNodes.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element) node;

                if (childElement.getTagName().equals("subject")) {
                    subject = childElement.getTextContent().trim().toLowerCase().replaceAll(" ", "_");

                }
                if (childElement.getTagName().equals("object")) {
                    object = childElement.getTextContent().trim().toLowerCase().replaceAll(" ", "_");

                }
            }
        }

        String result = NegEx.detect(sentence);

        if (result.equals("PREN") || result.equals("POST")) {
            if (object != null) {
                Double object_sentiment_value = handler.extract(object, "NN");
                if (object_sentiment_value != null) {
                    if (object_sentiment_value < 0 && averageValue < 0) {
                        return (-1 * averageValue);
                    } else if(object_sentiment_value > 0 && averageValue > 0){
                        return (-1 * averageValue);
                    }
                    else{
                        return  averageValue;
                    }
                }
                return  averageValue;
            }
            return averageValue;
        } else  if (result.equals("POPSEU") ||  result.equals("PSEU")) {
            if (object != null) {
                Double object_sentiment_value = handler.extract(object, "NN");
                if (object_sentiment_value != null) {
                    if (object_sentiment_value < 0 && averageValue > 0) {
                        return (-1 * averageValue);
                    } else if (object_sentiment_value > 0 && averageValue < 0){
                        return (-1 * averageValue);
                    }else {
                        return  averageValue;
                    }
                }
                return averageValue;
            }
            return  averageValue;
        } else if (result.equals("NN")) {
            return averageValue;
        } else {
            return averageValue;
        }


    }
}
