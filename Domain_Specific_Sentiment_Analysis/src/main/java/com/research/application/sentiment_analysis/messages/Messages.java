package com.research.application.sentiment_analysis.messages;

public interface Messages {

    String negative_Message(String Domain,Double whole_document_sentiment_value);

    String positive_Message(String Domain,Double whole_document_sentiment_value);

    String neutral_Message(String Domain,Double whole_document_sentiment_value);


}
