package com.research.application.sentiment_analysis.result_generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.research.application.sentiment_analysis.messages.*;
import com.research.application.sentiment_analysis.model.Sentence;
import com.research.application.sentiment_analysis.model.Sentiment_Result;

public class Result_Generator_General implements Result_Generator {

    private ArrayList<Sentence> sentenceList = new ArrayList<>();
    private Messages wholeDocumentMessage;
    private HashMap<String, Double> sentimentValuesHashMap = new HashMap<String, Double>();

    @Override
    public Sentiment_Result generate_result(HashMap<String, HashMap<String, Double>> hashMap, String type, String DOMAIN) {

        if (DOMAIN.equals("SentiWordNet")) {
            wholeDocumentMessage = new WholeDocumentMessageSentiwordNet();
        } else {
            wholeDocumentMessage = new WholeDocumentMessage();
        }
        Sentiment_Result sentiment_Result = new Sentiment_Result();

        for (HashMap.Entry<String, HashMap<String, Double>> entry : hashMap.entrySet()) {

            Sentence sentence = new Sentence();
            String sentenceId = entry.getKey();

            sentence.setID(sentenceId);
            //
            Double sentimentValue = null;
            for (HashMap.Entry<String, Double> entrySentence : entry.getValue().entrySet()) {
                sentimentValue = entrySentence.getValue();
                sentence.setSentence(entrySentence.getKey());
                sentence.setSentimentValue(sentimentValue);

                sentenceList.add(sentence);


            }

            sentimentValuesHashMap.put(sentenceId, sentimentValue);


        }

        // finalize whole document
        Double whole_document_sentiment_value = calculate_whole_document_sentiment_value(sentimentValuesHashMap, sentiment_Result);

        sentiment_Result.setWhole_sentiment_value(whole_document_sentiment_value);
        sentiment_Result.setType(type);
        if (whole_document_sentiment_value > 0) {
            sentiment_Result.setMessage(wholeDocumentMessage.positive_Message(DOMAIN, whole_document_sentiment_value));
        } else if (whole_document_sentiment_value < 0) {
            sentiment_Result.setMessage(wholeDocumentMessage.negative_Message(DOMAIN, whole_document_sentiment_value));
        } else {
            sentiment_Result.setMessage(wholeDocumentMessage.neutral_Message(DOMAIN, whole_document_sentiment_value));
        }


        Collections.sort(sentenceList, new Comparator<Sentence>() {
            @Override
            public int compare(Sentence o1, Sentence o2) {
                return o1.getID().compareTo(o2.getID());
            }
        });
        sentiment_Result.setSentenceList(sentenceList);
        // sentiment_Result.setParagraphs_sentiment_values(sentimentValuesHashMap);
        return sentiment_Result;
    }


    @Override
    public double calculate_whole_document_sentiment_value(HashMap<String, Double> hashMap, Sentiment_Result sentiment_result) {
        Double total = 0.0;
        int count = 0;
        int positive_count = 0;
        int negative_count = 0;
        int neutral_count = 0;
        for (HashMap.Entry<String, Double> entry : hashMap.entrySet()) {
            double value = entry.getValue();
            if (value > 0) {
                positive_count++;
            } else if (value < 0) {
                negative_count++;
            } else {
                neutral_count++;
            }
            total = total + value;
            count++;
        }

        sentiment_result.setPostive_Probability(calculate_Positive_Probability(positive_count, count));
        sentiment_result.setNegative_Probability(calculate_Negative_Probability(negative_count, count));
        sentiment_result.setNeutral_Probability(calculate_Neutral_Probability(neutral_count, count));
        return total / (double) count;
    }

    @Override
    public double calculate_Positive_Probability(int positive_count, int totalcount) {


        return positive_count / (double) totalcount;
    }

    @Override
    public double calculate_Negative_Probability(int negative_count, int totalcount) {


        return negative_count / (double) totalcount;
    }

    @Override
    public double calculate_Neutral_Probability(int neutral_count, int totalcount) {


        return neutral_count / (double) totalcount;
    }
}
