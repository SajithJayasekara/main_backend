package com.research.application.sentiment_analysis.messages;

public class WholeDocumentMessageSentiwordNet implements  Messages {
    @Override
    public String negative_Message(String Domain, Double whole_document_sentiment_value) {
        return "The sentiment value of whole document is " + whole_document_sentiment_value + ". Which means, generally, this document has negative sentiment.";
    }

    @Override
    public String positive_Message(String Domain, Double whole_document_sentiment_value) {
        return "The sentiment of whole document value is " + whole_document_sentiment_value + ". Which means, generally, this document has positive sentiment.";
    }

    @Override
    public String neutral_Message(String Domain, Double whole_document_sentiment_value) {
        return "The sentiment value of whole document is " + whole_document_sentiment_value + ". Which means, generally, this document has neutral sentiment.";
    }
}
