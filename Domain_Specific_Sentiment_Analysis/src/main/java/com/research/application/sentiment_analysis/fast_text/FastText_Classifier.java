package com.research.application.sentiment_analysis.fast_text;

import com.github.jfasttext.JFastText;

public class FastText_Classifier {

    static JFastText jft = new JFastText();

    public void create_model() {

        jft.runCmd(new String[]{
                "supervised",
                "-input", "fast_text_dataset.txt", //fast_text_dataset_2.txt
                "-output", "supervised.model"
        });


    }

    public static void loadModel() {
        jft.loadModel("supervised.model.bin");
        //jft.test("fast_text_dataset_2.txt");

    }

    /*
    * text classifier pass sentence then get label
    * */
    public static String classify(String text) {


        JFastText.ProbLabel probLabel = jft.predictProba(text);

        return probLabel.label;
    }

}
