package com.research.application.sentiment_analysis.analyser;

import com.research.application.sentiment_analysis.SentiWordNet_handler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class SentiwordNet_Analyser implements Analyser {

    private String XML_PATH;
    private SentiWordNet_handler sentiWordNet_handler;

    // string for paragraph id, internal HashMap sentence id and sentiment
    // values of them
    HashMap<String, HashMap<String, Double>> sentimentsHashMap = new HashMap<String, HashMap<String, Double>>();

    public SentiwordNet_Analyser(String SentiWordNet_PATH, String xML_PATH) {
        this.XML_PATH = xML_PATH;
        sentiWordNet_handler = new SentiWordNet_handler(SentiWordNet_PATH);
    }

    @Override
    public HashMap<String, HashMap<String, Double>> analyse() {
        File file = new File(XML_PATH);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        doc.getDocumentElement().normalize();

        // System.out.println("Root element :" +
        // doc.getDocumentElement().getNodeName());

        NodeList sentenceContenthList = doc.getElementsByTagName("sentenceContent");

        //ArrayList<String> paragraphIdList = new ArrayList<String>();
        for (int i = 0; i < sentenceContenthList.getLength(); i++) {
            Node nNode = sentenceContenthList.item(i);
            Element paraElement = (Element) nNode;
            String sentenceContentId = paraElement.getAttribute("id");
            //paragraphIdList.add(paragraphId);
            NodeList ContentList = nNode.getChildNodes();

            String sentence = null;
            String sentenceId = null;
            Element wordList = null;
            //HashMap<String, Double> sentencesHashMap = new HashMap<String, Double>();

            // ArrayList<String> sentenceIDList = new ArrayList<String>();

            for (int j = 0; j < ContentList.getLength(); j++) {

                Node contentNode = ContentList.item(j);

                if (contentNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element contentElement = (Element) contentNode;

                    if (contentElement.getTagName().equals("sentence")) {
                        // sentenceId = contentElement.getAttribute("id");
                        sentence = contentElement.getTextContent().trim();
                    }

                    if (contentElement.getTagName().equals("wordlist")) {
                        wordList = contentElement;
                    }

                    if (sentence != null && wordList != null) {

                        Double sentimentValue;
                        HashMap<String, Double> sentencesHashMap = new HashMap<String, Double>();
                        sentimentValue = sentiWordNetCalculation(wordList);
                        sentencesHashMap.put(sentence, sentimentValue);

                        sentimentsHashMap.put(sentenceContentId, sentencesHashMap);
                    }

                }
            }


        }
        return sentimentsHashMap;
    }

    public Double sentiWordNetCalculation(Element element) {
        Double avgValue = null;
        Double total = null;
        int wordCount = 0;
        NodeList wordList = element.getChildNodes();

        for (int i = 0; i < wordList.getLength(); i++) {

            Node wordNode = wordList.item(i);
            if (wordNode.getNodeType() == Node.ELEMENT_NODE) {

                Element wordElement = (Element) wordNode;
                String pos = wordElement.getAttribute("pos");

                String word = wordElement.getTextContent().trim().toLowerCase();

                //System.out.println(word);
                if (sentiWordNet_handler.extract(word, pos) != null) {
                    if (total == null) {
                        total = 0.0;
                    }
                    total = total + sentiWordNet_handler.extract(word, pos);
                    wordCount++;
                }

            }

        }

        if (total != null && wordCount != 0) {
            avgValue = total / (double) wordCount;
        }

        return avgValue;
    }

}
