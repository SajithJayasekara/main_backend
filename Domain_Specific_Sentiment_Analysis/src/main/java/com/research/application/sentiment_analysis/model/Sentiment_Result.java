package com.research.application.sentiment_analysis.model;

import java.util.ArrayList;

public class Sentiment_Result {

    private double whole_sentiment_value;
    private String type;
    private String message;
    private double postive_Probability;
    private double negative_Probability;
    private double neutral_Probability;
    private ArrayList<Sentence> sentenceList;


    public double getWhole_sentiment_value() {
        return whole_sentiment_value;
    }

    public void setWhole_sentiment_value(double whole_sentiment_value) {
        this.whole_sentiment_value = whole_sentiment_value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getPostive_Probability() {
        return postive_Probability;
    }

    public void setPostive_Probability(double postive_Probability) {
        this.postive_Probability = postive_Probability;
    }

    public double getNegative_Probability() {
        return negative_Probability;
    }

    public void setNegative_Probability(double negative_Probability) {
        this.negative_Probability = negative_Probability;
    }

    public double getNeutral_Probability() {
        return neutral_Probability;
    }

    public void setNeutral_Probability(double neutral_Probability) {
        this.neutral_Probability = neutral_Probability;
    }

    public ArrayList<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(ArrayList<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }


}
