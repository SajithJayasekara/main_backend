package com.research.application.sentiment_analysis.analyser;

import java.util.HashMap;

public interface Analyser {

    HashMap<String, HashMap<String, Double>> analyse();
}
