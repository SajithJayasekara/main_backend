package com.research.application.sentiment_analysis.result_generator;

import java.util.HashMap;

import com.research.application.sentiment_analysis.model.Sentiment_Result;

public interface Result_Generator {

    Sentiment_Result generate_result(HashMap<String, HashMap<String, Double>> hashMap, String type, String DOMAIN);

    double calculate_whole_document_sentiment_value(HashMap<String, Double> hashMap, Sentiment_Result sentiment_result);

    double calculate_Positive_Probability(int positive_count, int totalcount);

    double calculate_Negative_Probability(int negative_count, int totalcount);

    double calculate_Neutral_Probability(int neutral_count, int totalcount);

}
