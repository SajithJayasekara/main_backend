package com.research.application.sentiment_analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SentiWordNet_handler {

    private Map<String, Double> dictionary;

    public SentiWordNet_handler(String pathToSWN) {
        // This is main dictionary representation
        dictionary = new HashMap<String, Double>();

        // From String to list of doubles.
        HashMap<String, HashMap<Integer, Double>> tempDictionary = new HashMap<String, HashMap<Integer, Double>>();

        BufferedReader csv = null;
        try {
            csv = new BufferedReader(new FileReader(pathToSWN));
            int lineNumber = 0;

            String line;
            while ((line = csv.readLine()) != null) {
                lineNumber++;

                // If it's a comment, skip this line.
                if (!line.trim().startsWith("#")) {
                    // We use tab separation
                    String[] data = line.split("\t");

                    //wordTypeMarker is a POS Tag
                    String wordTypeMarker = data[0];

                    // Example line:
                    // POS ID PosS NegS SynsetTerm#sensenumber Description
                    // a 00009618 0.5 0.25 spartan#4 austere#3 ascetical#2
                    // ascetic#2 practicing great self-denial;...etc

                    // Is it a valid line? Otherwise, through exception.
                    if (data.length != 6) {
                        throw new IllegalArgumentException("Incorrect tabulation format in file, line: " + lineNumber);
                    }

                    //data[1] is id

                    // Calculate synset score as score = PosS - NegS per line
                    Double synsetScore = Double.parseDouble(data[2]) - Double.parseDouble(data[3]);

                    // Get all Synset terms ex: spartan#4 austere#3 ascetical#2
                    String[] synTermsSplit = data[4].split(" ");

                    // Go through all terms of current synset.
                    for (String synTermSplit : synTermsSplit) {
                        // Get synterm and synterm rank
                        // the array would be like this ex: [spartan,4]
                        String[] synTermAndRank = synTermSplit.split("#");
                        //ex synTerm => spartan#a
                        String synTerm = synTermAndRank[0] + "#" + wordTypeMarker;

                        int synTermRank = Integer.parseInt(synTermAndRank[1]);
                        // What we get here is a map of the type:
                        // term -> {score of synset#1, score of synset#2...}

                        // Add map to term if it doesn't have one
                        if (!tempDictionary.containsKey(synTerm)) {
                            //add thses details to tempDictionary
                            tempDictionary.put(synTerm, new HashMap<Integer, Double>());
                        }

                        // Add synset link to synterm
                        // for and temporary store main synsetScore Synset per line
                        tempDictionary.get(synTerm).put(synTermRank, synsetScore);
                    }
                }
            }

            // Go through all the terms.
            // calculate score according to word ranking
            for (Map.Entry<String, HashMap<Integer, Double>> entry : tempDictionary.entrySet()) {
                String word = entry.getKey();
                Map<Integer, Double> synSetScoreMap = entry.getValue();

                /*
                * Calculate weighted average. Weight the synsets according to
                * their rank.
                * Score= 1/2*first + 1/3*second + 1/4*third ..... etc.
                * Sum = 1/1 + 1/2 + 1/3 ...
                * note if some time word in multiple line
                * */

                double score = 0.0;
                double sum = 0.0;
                for (Map.Entry<Integer, Double> setScore : synSetScoreMap.entrySet()) {
                    // main synsetScore divided by rank
                    score += setScore.getValue() / (double) setScore.getKey();
                    sum += 1.0 / (double) setScore.getKey();
                }
                score /= sum;

                dictionary.put(word, score);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (csv != null) {
                try {
                    csv.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Double extract(String word, String POS) {
        if (POS.contains("NN") || POS.contains("NNS") || POS.contains("NNP") || POS.contains("NNPS"))
            return dictionary.get(word + "#n");
        else if (POS.contains("VB") || POS.contains("VBD") || POS.contains("VBG") || POS.contains("VBN")
                || POS.contains("VBP") || POS.contains("VBZ"))
            return dictionary.get(word + "#v");
        else if (POS.contains("JJ") || POS.contains("JJR") || POS.contains("JJS"))
            return dictionary.get(word + "#a");
        else if (POS.contains("RB") || POS.contains("RBR") || POS.contains("RBS"))
            return dictionary.get(word + "#r");
        else
            return null;
    }

}
