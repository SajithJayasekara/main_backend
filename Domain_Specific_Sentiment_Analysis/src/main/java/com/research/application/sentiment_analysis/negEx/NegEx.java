package com.research.application.sentiment_analysis.negEx;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * PREN - Prenegation rule tag, ex: not increase
 * POST - Postnegation rule tag, ex: is not increased
 * PSEU - pre Pseudo negation tag, ex: not decrease
 * POSEU - post Pseudo negation tag, ex: is not decreased
 * NN - no negation tag, ex: not use
 * */


public class NegEx {


    public static String detect(String sentenceString) {

        ArrayList rules = new ArrayList();
        ArrayList sortedRules;
        File ruleFile = new File("negex_triggers.txt");
        try {
            Scanner sc = new Scanner(ruleFile);


            while (sc.hasNextLine()) {
                rules.add(sc.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        Sorter s = new Sorter();
        sortedRules = s.sortRules(rules);

        String sentence = "." + sentenceString + ".";
        String filler = "_";
        Iterator iRule = sortedRules.iterator();
        while (iRule.hasNext()) {
            String rule = (String) iRule.next();
            Pattern p = Pattern.compile("[\\t]+");
            //split using tab
            String[] ruleTokens = p.split(rule.trim());
            // Add the regular expression characters to tokens and asemble the rule again.
            String[] ruleMembers = ruleTokens[0].trim().split(" ");
            String rule2 = "";
            for (int i = 0; i <= ruleMembers.length - 1; i++) {
                if (!ruleMembers[i].equals("")) {
                    if (ruleMembers.length == 1) {
                        rule2 = ruleMembers[i];
                        //not
                        //cause etc
                    } else {
                        rule2 = rule2 + ruleMembers[i].trim() + "\\s+";

                        //sufficient\s+to\s+rule\s+the\s+ <- output
                    }
                }
            }
            //remove last s+
            if (rule2.endsWith("\\s+")) {
                rule2 = rule2.substring(0, rule2.lastIndexOf("\\s+"));
                //sufficient\s+to\s+rule\s+the\s+ to sufficient\s+to\s+rule\s+the

            }

            rule2 = "(?m)(?i)[[\\p{Punct}&&[^\\]\\[]]|\\s+](" + rule2 + ")[[\\p{Punct}&&[^_]]|\\s+]";

            //if identify pattern in sentence surround with tag  ruleTokens[1] is the tag
            Pattern p2 = Pattern.compile(rule2.trim());
            Matcher m = p2.matcher(sentence);
            while (m.find() == true) {
                sentence = m.replaceAll(" " + ruleTokens[1].trim()
                        + m.group().trim().replaceAll(" ", filler)
                        + ruleTokens[1].trim() + " ");
            }


        }

        Pattern pSpace = Pattern.compile("[\\s+]");
        String[] sentenceTokens = pSpace.split(sentence);


        // Check for [PREN]
        for (int i = 0; i < sentenceTokens.length; i++) {

            if (sentenceTokens[i].trim().startsWith("[PREN]")) {
                return "PREN";
            }
        }

        for (int i = 0; i < sentenceTokens.length; i++) {

            if (sentenceTokens[i].trim().startsWith("[POST]")) {
                return "POST";
            }
        }

        for (int i = 0; i < sentenceTokens.length; i++) {

            if (sentenceTokens[i].trim().startsWith("[POPSEU]")) {
                return "POPSEU";
            }
        }

        for (int i = 0; i < sentenceTokens.length; i++) {

            if (sentenceTokens[i].trim().startsWith("[PSEU]")) {
                return "PSEU";
            }
        }

        for (int i = 0; i < sentenceTokens.length; i++) {

            if (sentenceTokens[i].trim().startsWith("[NN]")) {
                return "NN";
            }
        }

        return "not catch";
    }
}
