package com.research.application.sentiment_analysis.analyser;

import com.research.application.ontology.Neo4j_Configuratation;
import com.research.application.ontology.data_retrieve_dao.Data_Retrieve;
import com.research.application.sentiment_analysis.SentiWordNet_handler;
import com.research.application.sentiment_analysis.fast_text.FastText_Classifier;
import com.research.application.sentiment_analysis.negEx.NegExValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class Hybrid_Analyser implements Analyser {

    private String XML_PATH;
    private SentiWordNet_handler sentiWordNet_handler;
    private String ONTOLOGY;
    private String FASTTEXT;
    private Data_Retrieve data_retrieve;
    private String label;
    private String predictedLabel;


    /*
     * string for sentence id, internal HashMap (sentence  and sentiment values of them)
     * */
    HashMap<String, HashMap<String, Double>> sentimentsHashMap = new HashMap<String, HashMap<String, Double>>();

    // constructor
    public Hybrid_Analyser(String SentiWordNet_PATH, String xML_PATH, String ontology, String fastText) {

        this.XML_PATH = xML_PATH;
        sentiWordNet_handler = new SentiWordNet_handler(SentiWordNet_PATH);
        ONTOLOGY = ontology;
        FASTTEXT = fastText;
        data_retrieve = new Data_Retrieve();
        Neo4j_Configuratation.setDriver();
        Data_Retrieve.setDriver();


    }

    // main method
    @Override
    public HashMap<String, HashMap<String, Double>> analyse() {

        //set session
        data_retrieve.setSession();
        label = "__label__" + ONTOLOGY.toLowerCase().trim();


        File file = new File(XML_PATH);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        doc.getDocumentElement().normalize();

        // get sentenceContent element list from xml doc
        NodeList sentenceContentList = doc.getElementsByTagName("sentenceContent");


        for (int j = 0; j < sentenceContentList.getLength(); j++) {

            Node sentContentNode = sentenceContentList.item(j);

            /*
             * get child elements of sentenceContent
             * */
            if (sentContentNode.getNodeType() == Node.ELEMENT_NODE) {

                Element sentContentElement = (Element) sentContentNode;

                NodeList contentList = sentContentElement.getChildNodes();


                String sentence = null;
                String sentenceId = null;
                Element wordList = null;
                Element sentencePatterns = null;
                Element subjobj = null;

                for (int k = 0; k < contentList.getLength(); k++) {

                    Node contentNode = contentList.item(k);

                    if (contentNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element contentEliment = (Element) contentNode;

                        if (contentEliment.getTagName().equals("sentence")) {
                            sentenceId = contentEliment.getAttribute("id");
                            sentence = contentEliment.getTextContent().trim().replaceAll("\t", "").replaceAll("\n", "");
                        }

                        if (contentEliment.getTagName().equals("wordlist")) {
                            wordList = contentEliment;
                        }
                        if (contentEliment.getTagName().equals("subjobj")) {
                            subjobj = contentEliment;
                            //System.out.println(subjobj.getTagName());
                        }
                        if (contentEliment.getTagName().equals("sentencePatterns")) {
                            sentencePatterns = contentEliment;
                        }
                        // integrate fastTest
                        if (wordList != null && sentencePatterns != null && sentence != null &&
                                sentenceId != null && subjobj != null) {
                            Double sentimentValue;
                            HashMap<String, Double> sentencesHashMap = new HashMap<String, Double>();
                            if (FASTTEXT.equals("true")) {
                                predictedLabel = FastText_Classifier.classify(sentence);

                                if (predictedLabel.equals(label) || predictedLabel.equals("__label__both")) {
                                    // sentiment analysis using ontology
                                    sentimentValue = ontologyCalculation(sentencePatterns);
                                    if (sentimentValue != null) {
                                        sentencesHashMap.put(sentence, sentimentValue);
                                    } else {
                                        // sentiment analysis using sentiWordNet
                                        sentimentValue = sentiWordNetCalculation(wordList, sentence, subjobj);
                                        sentencesHashMap.put(sentence, sentimentValue);
                                    }
                                } else {
                                    // sentiment analysis using sentiWordNet
                                    sentimentValue = sentiWordNetCalculation(wordList, sentence, subjobj);
                                    sentencesHashMap.put(sentence, sentimentValue);
                                }
                            } else {
                                sentimentValue = ontologyCalculation(sentencePatterns);
                                if (sentimentValue != null) {
                                    sentencesHashMap.put(sentence, sentimentValue);
                                } else {
                                    sentimentValue = sentiWordNetCalculation(wordList, sentence, subjobj);
                                    sentencesHashMap.put(sentence, sentimentValue);
                                }
                            }


                            sentimentsHashMap.put(sentenceId, sentencesHashMap);
                        }

                    }
                }
            }
        }
        // close session
        data_retrieve.closeSession();
        Data_Retrieve.closeDriver();
        return sentimentsHashMap;
    }

    /*
     * main method for sentiment analysis using ontology
     * */
    public Double ontologyCalculation(Element element) {

        Double value = null;
        int count = 0;
        Double averageValue = null;
        //get pattern list of  a sentence
        if (element.hasChildNodes()) {
            NodeList patternList = element.getChildNodes();
            for (int i = 0; i < patternList.getLength(); i++) {

                Node patternNode = patternList.item(i);
                if (patternNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element patternElement = (Element) patternNode;

                    NodeList patternContentList = patternElement.getChildNodes();
                    Element object = null;
                    Element relation = null;
                    Double extractValue;
                    for (int j = 0; j < patternContentList.getLength(); j++) {

                        Node contentNode = patternContentList.item(j);

                        if (contentNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element contentElement = (Element) contentNode;

                            if (contentElement.getTagName().equals("noun")) {

                                object = contentElement;
                            }

                            if (contentElement.getTagName().equals("relation")) {

                                relation = contentElement;
                            }

                            if (object != null && relation != null) {

                                String objectValue = object.getTextContent().trim().toLowerCase();
                                String relationShipValue = relation.getTextContent().trim().toLowerCase();
                                extractValue = getOntologySentimentValue(ONTOLOGY, objectValue, relationShipValue);
                                if (extractValue != null) {
                                    if (value == null) {
                                        value = 0.0;
                                    }
                                    value = value + extractValue;
                                    count++;
                                }

                            }
                        }

                    }
                }

            }
        } else {
            return null;
        }

        if (value != null && count != 0) {
            averageValue = value / (double) count;

        }
        return averageValue;
    }

    /*
     * method for sentiment analysis using sentiWordNet
     * */
    public Double sentiWordNetCalculation(Element element, String sentence, Element subObj) {
        Double avgValue = 0.0;
        Double total = null;
        int wordCount = 0;
        NodeList wordList = element.getChildNodes();

        for (int i = 0; i < wordList.getLength(); i++) {

            Node wordNode = wordList.item(i);
            if (wordNode.getNodeType() == Node.ELEMENT_NODE) {

                Element wordElement = (Element) wordNode;
                String pos = wordElement.getAttribute("pos");

                String word = wordElement.getTextContent().trim().toLowerCase();

                if (sentiWordNet_handler.extract(word, pos) != null) {
                    if (total == null) {
                        total = 0.0;
                    }
                    total = total + sentiWordNet_handler.extract(word, pos);
                    wordCount++;
                }

            }

        }

        if (total != null && wordCount != 0) {
            avgValue = total / (double) wordCount;
            //perform Negex Validation
            avgValue = NegExValidator.negexValidation(sentence, avgValue, subObj, sentiWordNet_handler);

        }

        return avgValue;
    }

    // sub-method for sentiment analysis using ontology
    public Double getOntologySentimentValue(String DOMAIN, String objectValue, String relationShipValue) {
        Double value = null;
        if (relationShipValue.equals("increase")) {
            Double extractValue = data_retrieve.getIncrease(DOMAIN, objectValue);
            if (extractValue != null) {
                value = extractValue;
            }

        } else if (relationShipValue.equals("decrease")) {
            Double extractValue = data_retrieve.getDecrease(DOMAIN, objectValue);
            if (extractValue != null) {
                value = extractValue;
            }
        }
        return value;
    }

}
