package com.research.application.sentiment_analysis.model;

public class Sentence {

    private String id;
    private Double sentimentValue;
    private String sentence;

    public String getID() {
        return id;
    }

    public void setID(String iD) {
        id = iD;
    }

    public Double getSentimentValue() {
        return sentimentValue;
    }

    public void setSentimentValue(Double sentimentValue) {
        this.sentimentValue = sentimentValue;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String message) {
        this.sentence = message;
    }

}

